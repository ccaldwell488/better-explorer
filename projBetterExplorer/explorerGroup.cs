﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace projBetterExplorer
{
    class explorerGroup
    {
        public string CurrentPath { get; private set; }
        public string PreviousPath { get; private set; }
        private FlowLayoutPanel flpCustom;
        private GroupBox gbCustom;

        enum viewTypes
        {
            list, icons, tiles, thumbnails
        }

        public Boolean showSizeInfo { get; private set; }

        public explorerGroup(string path)
        {
            CurrentPath = path;
            showSizeInfo = true;
            //            currentView = viewTypes.list;
        }

        public void addGroupBox(FlowLayoutPanel flpMain)
        {
            flpCustom = new FlowLayoutPanel();
            gbCustom = new GroupBox();

            flpCustom.AutoScroll = true;
            flpCustom.Dock = DockStyle.Fill;

            gbCustom.Paint -= paintGroupBox;
            gbCustom.Paint += paintGroupBox;

            gbCustom.Size = new Size(flpMain.Size.Width / 2 - 10 , flpMain.Size.Height / 2);
            gbCustom.ForeColor = Color.Black;
            gbCustom.Text = CurrentPath;

            scanForItems(flpCustom , false);

            gbCustom.Controls.Add(flpCustom);
            flpMain.Controls.Add(gbCustom);
        }

        private void scanForItems(FlowLayoutPanel flp , Boolean filesScanned)
        {
            if (!filesScanned)
            {
                flp.Controls.Clear();
            }

            // Scan for files or for folders depending on flag
            var items = filesScanned ? Directory.GetDirectories(CurrentPath) : Directory.GetFiles(CurrentPath);

            foreach (string item in items)
            {
                var pb = new PictureBox();
                pb.Paint -= paintPictureBox;
                pb.Paint += paintPictureBox;
                pb.Margin = new Padding(0);
                pb.Width = 195;

                var foo = new frmBE();
                pb.BackColor = filesScanned ? foo.cFolder : foo.cFile;
                //Filename/Foldername
                pb.Text = item.Substring(item.LastIndexOf('\\') + 1);
                pb.Tag = item;
                pb.Click -= new EventHandler(itemClick);
                pb.Click += new EventHandler(itemClick);
                flp.Controls.Add(pb);
            }

            if (!filesScanned)
            {
                scanForItems(flp , true);
            }
        }

        private void itemClick(object sender , EventArgs e)
        {
            var item = (PictureBox)sender;
            var path = item.Tag.ToString();
            if (Directory.Exists(path))
            {
                CurrentPath = path;
                scanForItems(flpCustom , false);
            }
            else
            {
                Process.Start(path);
            }
        }

        private void paintPictureBox(object sender , PaintEventArgs e)
        {
            var pb = (PictureBox)sender;

            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

            SizeF textSize = e.Graphics.MeasureString(pb.Text , frmBE.DefaultFont);
            int xPos = (pb.Width / 2) - ((int)textSize.Width / 2);
            int yPos = (pb.Height / 2) - ((int)textSize.Height / 2);
            e.Graphics.DrawString(pb.Text , pb.Font , Brushes.White , xPos , yPos);
        }

        private void paintGroupBox(object sender , PaintEventArgs e)
        {
            var gb = (GroupBox)sender;
            e.Graphics.Clear(Color.FromArgb(140 , 140 , 140));
            e.Graphics.DrawString(gb.Text , gb.Font , Brushes.White , 0 , 0);
        }

        //TODO:
        /*
        Error handling
        drag and drop events instead of clicking add button
        custom box with static items through drag and drop
        */
    }

}

