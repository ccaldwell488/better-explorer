﻿namespace projBetterExplorer
{
    partial class frmBE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exitApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofdAddFolder = new System.Windows.Forms.OpenFileDialog();
            this.btnEnlarge = new System.Windows.Forms.PictureBox();
            this.btnMini = new System.Windows.Forms.PictureBox();
            this.btnEdit = new System.Windows.Forms.PictureBox();
            this.btnAddGame = new System.Windows.Forms.PictureBox();
            this.contextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEnlarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddGame)).BeginInit();
            this.SuspendLayout();
            // 
            // flowPanel
            // 
            this.flowPanel.AutoScroll = true;
            this.flowPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(137)))), ((int)(((byte)(216)))));
            this.flowPanel.Location = new System.Drawing.Point(0, 45);
            this.flowPanel.Name = "flowPanel";
            this.flowPanel.Size = new System.Drawing.Size(850, 433);
            this.flowPanel.TabIndex = 0;
            this.flowPanel.MouseEnter += new System.EventHandler(this.flowPanel_MouseEnter);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitApplicationToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(166, 26);
            // 
            // exitApplicationToolStripMenuItem
            // 
            this.exitApplicationToolStripMenuItem.Name = "exitApplicationToolStripMenuItem";
            this.exitApplicationToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.exitApplicationToolStripMenuItem.Text = "Exit Application";
            this.exitApplicationToolStripMenuItem.Click += new System.EventHandler(this.exitApplicationToolStripMenuItem_Click);
            // 
            // ofdAddFolder
            // 
            this.ofdAddFolder.FileName = "openFileDialog1";
            // 
            // btnEnlarge
            // 
            this.btnEnlarge.Image = global::projBetterExplorer.Properties.Resources.enlarge;
            this.btnEnlarge.Location = new System.Drawing.Point(765, 6);
            this.btnEnlarge.Name = "btnEnlarge";
            this.btnEnlarge.Size = new System.Drawing.Size(33, 33);
            this.btnEnlarge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEnlarge.TabIndex = 37;
            this.btnEnlarge.TabStop = false;
            this.btnEnlarge.Click += new System.EventHandler(this.btnEnlarge_Click);
            this.btnEnlarge.MouseEnter += new System.EventHandler(this.btnEnlarge_MouseEnter);
            this.btnEnlarge.MouseLeave += new System.EventHandler(this.btnEnlarge_MouseLeave);
            // 
            // btnMini
            // 
            this.btnMini.Image = global::projBetterExplorer.Properties.Resources.menu;
            this.btnMini.Location = new System.Drawing.Point(805, 6);
            this.btnMini.Name = "btnMini";
            this.btnMini.Size = new System.Drawing.Size(33, 33);
            this.btnMini.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMini.TabIndex = 36;
            this.btnMini.TabStop = false;
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            this.btnMini.MouseEnter += new System.EventHandler(this.btnMini_MouseEnter);
            this.btnMini.MouseLeave += new System.EventHandler(this.btnMini_MouseLeave);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = global::projBetterExplorer.Properties.Resources.edit;
            this.btnEdit.Location = new System.Drawing.Point(51, 6);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(33, 33);
            this.btnEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnEdit.TabIndex = 34;
            this.btnEdit.TabStop = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            this.btnEdit.MouseEnter += new System.EventHandler(this.btnEdit_MouseEnter);
            this.btnEdit.MouseLeave += new System.EventHandler(this.btnEdit_MouseLeave);
            // 
            // btnAddGame
            // 
            this.btnAddGame.Image = global::projBetterExplorer.Properties.Resources.add;
            this.btnAddGame.Location = new System.Drawing.Point(12, 6);
            this.btnAddGame.Name = "btnAddGame";
            this.btnAddGame.Size = new System.Drawing.Size(33, 33);
            this.btnAddGame.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnAddGame.TabIndex = 33;
            this.btnAddGame.TabStop = false;
            this.btnAddGame.Click += new System.EventHandler(this.btnAddGame_Click);
            this.btnAddGame.MouseEnter += new System.EventHandler(this.btnAddGame_MouseEnter);
            this.btnAddGame.MouseLeave += new System.EventHandler(this.btnAddGame_MouseLeave);
            // 
            // frmBE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(137)))), ((int)(((byte)(216)))));
            this.ClientSize = new System.Drawing.Size(850, 478);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.btnEnlarge);
            this.Controls.Add(this.btnMini);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAddGame);
            this.Controls.Add(this.flowPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmBE";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Better Explorer";
            this.Load += new System.EventHandler(this.frmBE_Load);
            this.contextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnEnlarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddGame)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.FlowLayoutPanel flowPanel;
        internal System.Windows.Forms.PictureBox btnAddGame;
        internal System.Windows.Forms.PictureBox btnEdit;
        internal System.Windows.Forms.PictureBox btnEnlarge;
        internal System.Windows.Forms.PictureBox btnMini;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem exitApplicationToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog ofdAddFolder;
    }
}

