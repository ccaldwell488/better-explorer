﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace projBetterExplorer
{
    public partial class frmBE : Form
    {
        public frmBE()
        {
            InitializeComponent();
        }

        //TODO: Display app at 1/4 screen size
        bool smallSize = true;

        public Color cBackground = Color.FromArgb(219 , 216 , 210);
        public Color cFolder = Color.FromArgb(83 , 80 , 158);
        public Color cFile = Color.FromArgb(81 , 183 , 191);

        List<explorerGroup> arrGroups = new List<explorerGroup>();

        private void frmBE_Load(object sender , EventArgs e)
        {
            // Set icon, colour and position
            Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            BackColor = cBackground;
            flowPanel.BackColor = cBackground;
            TopMost = true;
        }

        private void exitApplicationToolStripMenuItem_Click(object sender , EventArgs e)
        {
            Application.Exit();
        }

        private void btnMini_Click(object sender , EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void btnMini_MouseEnter(object sender , EventArgs e)
        {
            btnMini.Image = Properties.Resources.menuH;
        }

        private void btnMini_MouseLeave(object sender , EventArgs e)
        {
            btnMini.Image = Properties.Resources.menu;
        }

        private void btnAddGame_MouseEnter(object sender , EventArgs e)
        {
            btnAddGame.Image = Properties.Resources.addH;
        }

        private void btnAddGame_MouseLeave(object sender , EventArgs e)
        {
            btnAddGame.Image = Properties.Resources.add;
        }

        private void btnAddGame_Click(object sender , EventArgs e)
        {
            var fileBrowser = new FolderBrowserDialog();
            DialogResult result = fileBrowser.ShowDialog();

            if (result == DialogResult.OK)
            {
                var foo = new explorerGroup(fileBrowser.SelectedPath);
                foo.addGroupBox(flowPanel);
                arrGroups.Add(foo);
            }

        }

        private void btnEnlarge_MouseEnter(object sender , EventArgs e)
        {
            btnEnlarge.Image = Properties.Resources.enlargeH;
        }

        private void btnEnlarge_MouseLeave(object sender , EventArgs e)
        {
            btnEnlarge.Image = Properties.Resources.enlarge;
        }

        private void btnEnlarge_Click(object sender , EventArgs e)
        {
            //Debug.WriteLine(this.Size + "\n" + flowPanel.Size + "\n" + btnEnlarge.Location.ToString() + "\n" + btnMini.Location.ToString());

            if (smallSize)
            {
                int w = Screen.PrimaryScreen.Bounds.Width;
                int h = Screen.PrimaryScreen.Bounds.Height;

                WindowState = FormWindowState.Maximized;
                flowPanel.Size = Screen.PrimaryScreen.Bounds.Size;
                foreach (GroupBox control in flowPanel.Controls)
                {
                    control.Width = flowPanel.Width / 2 - 10;
                    control.Height = flowPanel.Height / 2;
                }
                btnEnlarge.Location = new Point(w - 80 , 6);
                btnMini.Location = new Point(w - 40 , 6);
                smallSize = !smallSize;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                Size = new Size(850 , 478);
                flowPanel.Size = new Size(850 , 433);
                foreach (GroupBox control in flowPanel.Controls)
                {
                    control.Width = (flowPanel.Size.Width / 2 - 10);
                    control.Height = (flowPanel.Size.Height / 2);
                }
                btnEnlarge.Location = new Point(765 , 6);
                btnMini.Location = new Point(805 , 6);
                smallSize = !smallSize;
            }

            //centerComponents();
        }

        private void btnEdit_MouseEnter(object sender , EventArgs e)
        {
            btnEdit.Image = Properties.Resources.editH;
        }

        private void btnEdit_MouseLeave(object sender , EventArgs e)
        {

            btnEdit.Image = Properties.Resources.edit;
        }

        private void btnEdit_Click(object sender , EventArgs e)
        {

        }

        private void flowPanel_MouseEnter(object sender , EventArgs e)
        {
            flowPanel.Focus();
        }

    }
}
